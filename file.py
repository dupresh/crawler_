import re
import urllib.request
import urllib.parse

START_LINK = "http://www.epam.com/"
AllLinks = list()
StatusAndLinkSet = set()
fileName = "crawler.txt"
setLinks = set()
setForFile = set()

class Crawler:
    @staticmethod
    def PageParser(pagereferance):
        try:
            page = urllib.request.urlopen(pagereferance)
            html = page.read()
            page.close()
            AllHref = re.findall('.*?href="(.*?)"', str(html))
            for href in AllHref:
                if (START_LINK + href) not in AllLinks:
                    if href.find('javascript:') and href.find('#icon') and href.find('http') and href.find('mailto')\
                            and href.find('.ico') and href.find('.png') and href.find('/etc') \
                            and href.find('intelligent-enterprise') and href.find('.css'):
                                AllLinks.append(START_LINK + href)
        except urllib.request.URLError:
            print('invalid url')

    @staticmethod
    def getStatus(pagereferance):
       code = urllib.request.urlopen(pagereferance).getcode()
       if code in [100, 307]:
           status = "working" + code
       else:
           status = "error" + code
       return status

    @staticmethod
    def AddToFile(StatusAndLinkSet):
        file = open(fileName, 'a')
        for stat in StatusAndLinkSet:
            file.write(stat)
        file.close()


def main():
    Crawler.PageParser(START_LINK)
    for links in AllLinks:
        Crawler.PageParser(links)
        print(links)
        setLinks.add(links)
    for link in setLinks:
        setForFile.add(link + Crawler.getStatus(link))
    Crawler.AddToFile(("\n".join(setForFile)))

if __name__ == "__main__":
    main()
